#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""EJERCICIO 1"""
import random
import time
import os


# clase que almacena estado de las ruedas
class Ruedas():

    # set que almacena el estado de las ruedas
    def setruedas(self, rueda1, rueda2, rueda3, rueda4):
        self.rueda1 = rueda1
        self.rueda2 = rueda2
        self.rueda3 = rueda3
        self.rueda4 = rueda4

    # set que almacena el desgaste random de las ruedas
    def setranruedas(self, ran1, ran2, ran3, ran4):
        rueda1 = self.rueda1 - (ran1 * self.rueda1)
        rueda2 = self.rueda2 - (ran2 * self.rueda2)
        rueda3 = self.rueda3 - (ran3 * self.rueda3)
        rueda4 = self.rueda4 - (ran4 * self.rueda4)
        # estas son nuevamente enviadas al set de almacenamiento de
        # estados de ruedas
        wheels.setruedas(rueda1, rueda2, rueda3, rueda4)
        self.rueda1 = rueda1
        self.rueda2 = rueda2
        self.rueda3 = rueda3
        self.rueda4 = rueda4

    # cada rueda con su get porque no funcionaba de otra manera :c
    def getrueda1(self):
        r1 = self.rueda1
        return r1

    def getrueda2(self):
        r2 = self.rueda2
        return r2

    def getrueda3(self):
        r3 = self.rueda3
        return r3

    def getrueda4(self):
        r4 = self.rueda4
        return r4

    # una gráfica del auto y sus ruedas en posición
    # estos porcentajes solo tienen un décimal
    def getruedas(self):
        a = self.rueda1
        b = self.rueda2
        c = self.rueda3
        d = self.rueda4
        print("     ----------")
        print("    |          |")
        print("   ", "{0:.1f}".format(a), "%  ", "{0:.1f}".format(b), "%")
        print("    |          |")
        print("    |          |")
        print("   ", "{0:.1f}".format(c), "%  ", "{0:.1f}".format(d), "%")
        print("    |          |")
        print("     ----------")


# clase que almacena los datos del vehículo
class Auto():

    # dato del cilindrado del motor
    def setmotor(self, motor):
        self.motor = motor

    # entrega el cilindrado
    def getmotor(self):
        return self.motor

    # estado del auto (apagado/encendido)
    def setestado(self, estado):
        if estado == 'a':
            estado = "apagado"
        elif estado == 'e':
            estado = "encendido"
        self.estado = estado

    def getestado(self):
        return self.estado
    # y el combustible (debí haberlo aprovechado de otra manera)

    def setcombustible(self, fuel):
        self.fuel = fuel

    def getcombustible(self):
        return self.fuel


# clase calcula la distancia
class Distance():

    # llegan 3 datos
    def set_estado(self, v, t, d):
        self.dis = (v * t) + d

    def get_estado(self):
        return self.dis


# clase que muetra la perdida de compustible cada cierto kilometraje
class Loss():

    # kilometros en los que baja la bencina
    def set_datos(self, m1, m2):
        self.m1 = m1
        self.m2 = m2

    # llega la distancia, tipo de motor y cantidad de combustible
    def set_perdida(self, d, m, fuel):
        m1 = self.m1
        m2 = self.m2
        # condición de cilindrado
        if m == 1.2:
            # condición de cantidad de kilometro
            if d >= m1 and d <= m1 + 1:
                # si está entre esos parámetros
                # la bencina baja 1
                fuel = fuel - 1.0
                # se duplica la siguiente meta
                m1 = m1 + 20
        elif m == 1.6:
            if d >= 20 and d <= m2 + 1:
                fuel = fuel - 1.0
                m2 = m2 + 14

        # se guardan los datos obtenidos
        self.fuel = fuel
        loss.set_datos(m1, m2)
        auto.setcombustible(fuel)

    # se envía el dato del combustible
    def get_perdida(self):
        return self.fuel


# mensaje final cuando se acaba la bencina
def f():
    print("\n---------------------------------------")
    print("  SE LE ACABÓ LA GASOLINA AMIGO :C\n")
    print("    ooooooooooo")
    print("    ooooooooooo")
    print("    oooo")
    print("    oooo")
    print("    ooooooooo")
    print("    ooooooooo")
    print("    oooo")
    print("    oooo")
    print("    oooo")
    print("    oooo")
    exit()


# función de datos al instante del recorrido del auto
def velocimetro():

    # datos importantes y variables
    fuel = auto.getcombustible()
    print (fuel)
    t = (random.randrange(1, 11)) / 3600
    m = auto.getmotor()
    d = distancia.get_estado()
    distancia.set_estado(v, t, d)
    d = distancia.get_estado()
    loss.set_perdida(d, m, fuel)
    gasolina = loss.get_perdida()
    # randoms del desgaste de las ruedas
    ran1 = (random.randrange(1, 11))/100
    ran2 = (random.randrange(1, 11))/100
    ran3 = (random.randrange(1, 11))/100
    ran4 = (random.randrange(1, 11))/100
    # se envían
    wheels.setranruedas(ran1, ran2, ran3, ran4)

    # imprime cada instante
    print("  Información del vehículo\n")
    print("   >Distancia recorrida: ", "{0:.2f}".format(d), "km")
    print("   >Quedan: ", "{0:.2f}".format(gasolina), "L de combustible")
    print("   >Estado de las ruedas:")
    wheels.getruedas()
    # ruedas
    r1 = wheels.getrueda1()
    r2 = wheels.getrueda2()
    r3 = wheels.getrueda3()
    r4 = wheels.getrueda4()
    rueda1 = r1
    rueda2 = r2
    rueda3 = r3
    rueda4 = r4
    try:
        # condiciones
        # cuando se desgasta hasta 90% una rueda
        # el programa da la opción de cambiarla
        # se apagrá el vehículo
        # este se debe encender y avanzar
        # por lo que perderá 1% del combustible
        if r1 <= 11:
            print("  Su rueda 1 se reventó")
            print("  Su apagó su vhículo")
            si = input(" Ingrese 's' para cambiar: ")
            while si != "s":
                si = input(" Ingrese 's' para cambiar: ")
            if si == "s":
                r1 = 100
                rueda1 = r1
                wheels.setruedas(rueda1, rueda2, rueda3, rueda4)
                si = input(" Ingrese 'v' para encender y avanzar: ")
                while si != "v":
                    si = input(" Ingrese 'v' para encender yavanzar: ")
                if si == "v":
                    fuel = fuel - (fuel * 0.001)
                    auto.setcombustible(fuel)
                    os.system('clear')
                    velocimetro()
        if r2 <= 11:
            print("  Su rueda 2 se reventó")
            print("  Su apagó su vhículo")
            si = input(" Ingrese 's' para cambiar: ")
            if si == "s":
                r2 = 100
                rueda2 = r2
                wheels.setruedas(rueda1, rueda2, rueda3, rueda4)
                si = input(" Ingrese 'v' para encender y avanzar: ")
                while si != "v":
                    si = input(" Ingrese 'v' para encender yavanzar: ")
                if si == "v":
                    fuel = fuel - (fuel * 0.001)
                    auto.setcombustible(fuel)
                    os.system('clear')
                    velocimetro()
        if r3 <= 11:
            print("  Su rueda 3 se reventó")
            print("  Su apagó su vhículo")
            si = input(" Ingrese 's' para cambiar: ")
            if si == "s":
                r3 = 100
                rueda3 = r3
                wheels.setruedas(rueda1, rueda2, rueda3, rueda4)
                si = input(" Ingrese 'v' para encender y avanzar: ")
                while si != "v":
                    si = input(" Ingrese 'v' para encender yavanzar: ")
                if si == "v":
                    fuel = fuel - (fuel * 0.001)
                    auto.setcombustible(fuel)
                    os.system('clear')
                    velocimetro()
        if r4 <= 11:
            print("  Su rueda 4 se reventó")
            print("  Su apagó su vhículo")
            si = input(" Ingrese 's' para cambiar: ")
            if si == "s":
                r4 = 100
                rueda4 = r4
                wheels.setruedas(rueda1, rueda2, rueda3, rueda4)
                si = input(" Ingrese 'v' para encender y avanzar: ")
                while si != "v":
                    si = input(" Ingrese 'v' para encender yavanzar: ")
                if si == "v":
                    fuel = fuel - (fuel * 0.001)
                    auto.setcombustible(fuel)
                    os.system('clear')
                    velocimetro()
    except ValueError:
        print(" Ingrese una opción válida")
        velocimetro()

    # si la gasolina llega a 0 o 1 se acaba
    if gasolina <= 1:
        f()

    # comandos para que se imprima varias veces
    time.sleep(0.1)
    os.system('clear')
    velocimetro()


# panel de control
def panel_control():
    # aqui se solicita al conductor que avance
    # o que apague el vehículo
    try:
        print("\n---------------------------------------")
        print("         PANEL DE CONTROL")
        print("         ****************")
        print("---------------------------------------")
        print(" Seleccione una opción")
        print(" 1) Apagar(a)")
        print(" 2) Avanzar (v)")
        print("---------------------------------------")
        opcion = input(" --> ")
        while opcion != "v" and opcion != "a":
            print(" Ingrese una opción válida")
            opcion = input(" --> ")
        if opcion == "a":
            print("  --APAGADO--")
            estado = opcion
            auto.setestado(estado)
            menu()
        elif opcion == "v":
            velocimetro()
    except ValueError:
        print(" Ingrese una opción válida")
        return panel_control()


# función para encender el auto
def encendido():
    if auto.getestado() == "apagado":
        opcion = input("   -Encienda su vehículo (e): ")
        while opcion != "e":
            opcion = input("   -Encienda su vehículo (e): ")
        if opcion == "e":
            os.system('clear')
            print("    Perfecto!")
            estado = opcion
            # el estado cambia
            auto.setestado(estado)
        print("---------------------------------------")
        print("  --ENCENDIDO--")
        estado = auto.getestado()
        gasolina = auto.getcombustible()
        # y se gasta 1% del combustible
        if estado == "encendido":
            fuel = gasolina - (gasolina * 0.001)
            auto.setcombustible(fuel)
            f = auto.getcombustible()
        print("  >Su estado de combustible es: ", "{0:.2f}".format(f), "L")


# función menú, muestra todos los datos al principio
def menu():
    print("---------------------------------------")
    print("  DATOS:")
    print("  -Estado:", auto.getestado())
    print("  -Cilindrada de motor: ", auto.getmotor())
    f = auto.getcombustible()
    print("  -Estado de combustible: ", "{0:.2f}".format(f), "L")
    print("  -Estado de ruedas:")
    wheels.getruedas()
    print("---------------------------------------")
    encendido()
    panel_control()

# función principal
# se guardan todos los datos y variables a usar
if __name__ == '__main__':
    print(" Usted se encuentra en una carretera recta con su auto apagado\n")
    auto = Auto()
    wheels = Ruedas()
    loss = Loss()
    # ~ pits = Alospits()
    distancia = Distance()
    motor = random.choice((1.2, 1.6))
    estado = "a"
    fuel = 32
    rueda1 = 100
    rueda2 = 100
    rueda3 = 100
    rueda4 = 100
    m1 = 20
    m2 = 14
    v = 120
    t = (random.randrange(1, 11)) / 3600
    d = 0
    distancia.set_estado(v, t, d)
    auto.setmotor(motor)
    auto.setestado(estado)
    auto.setcombustible(fuel)
    wheels.setruedas(rueda1, rueda2, rueda3, rueda4)
    loss.set_datos(m1, m2)
    menu()
